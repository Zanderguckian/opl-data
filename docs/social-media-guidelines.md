﻿**General policies**

***Posts:***

- Social media posts must be pre-approved by at least 50% of the social media team, with a minimum 1 hour cool off period to allow input from other parties. The social media team is defined as consisting of the following individuals:
  - Sean Stangl
  - Trystan Oakley
  - Jo Whiteley
  - Gem Lucas
  - Matt Pearce
  - Romi Basting

The approval process must be conducted in a medium in which the entire social media team is able to view it. Ideally this will be in a medium in which every contributor can view the decision process, however closed messaging may be used when necessary in cases where confidentiality is needed.

- Posts must not comment negatively on individual people or organisations,unless approved by all of the social media team

***Stories:***

- Social media stories that fit one or more of the following categories are automatically pre-approved, and no requirement for consultation nor cooling-off period applies:
  - Reposts of lifter rankings where the content is not intended to reflect negatively on a federation or lifter.
  - Announcements from federations where there is no conflict with the other social media guidelines.
  - Analysis of the OpenPowerlifting data set where the content is not intended to reflect negatively on a federation or lifter.
  - Content regarding improvement to the functionality of the OpenPowerlifting & OpenIPF websites and/or improvements to the corresponding datasets.
- All other social media stories must be pre-approved by at least 50% of the social media "team", with a minimum 1 hour cool off period to allow input from other parties.
- Self promotion is fine, eg sharing posts where a lifter is wearing official OPL apparel
- Stories must not comment negatively on individual people or organisations, unless approved by all of the social media team.

***Direct messaging:***

- All messaging when representing the OPL "brand" identifies who on the team they are talking to.
- In the case of an ongoing conversation, messaging when representing OPL is from a single person, unless the original person asks another team member to join the conversation. Conversations where there has been more than a week after the previous interaction may be responded to by a different person.
- Direct messaging should not be used to disparage individual people or organisations.

**OpenPowerlifting specific policies:**

- OpenPowerlifting content should not be seen to show favouritism towards federations, countries or individual lifters.
- OpenPowerlifting content should not make reference to the efficacy of drug-testing in organisations or the drug use of any athletes or organisations.

Contact with federations:

When a federation is contacted in regards to matters concerning OpenPowerlifting the team are to be informed of the contents of such communication.

**OpenPupperlifting specific policies:**

- No cats! 
- OpenPupperlifting should not be seen to show any puppers as not being good boys or girls.
- OpenPupperlifting should only show humans treating puppers with love, care, respect, and maybe treatos.


**OpenIPF specific policies:**

- OpenIPF content must not make reference to other federations.
- OpenIPF content must not make references to drug-testing, whether in the IPF or in other federations.
- OpenIPF content should avoid making references to banned individuals when at all possible.

Contact with the IPF:

Contact with the IPF should be via Emanuel Scheiber wherever possible and not be conducted over social media. The team should be informed of any contact with the IPF pertaining to the project.

